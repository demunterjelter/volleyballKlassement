# volleyballKlassement

## Feedback 22/01
- Klassendiagram 
	- verwijder getters en setters
	- voeg hoeveelheden toe (0, 1, 0..1, *, 0..*, 1..*)aan de relaties
	- speler en team hebben bidirectionele relatie, mag geen pijl zijn in de relaties
	- club en team idem als bovenstaande
	- attribuut TeamList zou met kleine letter moeten zijn bij club
	- attribuut GameList in Competition idem als bovenstaande
- code algemeen: verwijder gegenereerde template code, bv bovenaan in AbstractRepository
```java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
```
- GameRepository
	- verwijder ongebruikte imports
- AbstractPersistenceTest
	- verwijder ongebruikte imports
	- TODO of wel wijzigen naar commentaar of volledig weg
- CompetitionPersistenceTest
	- verwijder ongebruikte imports
- Aangezien kleine gegevens basis, Services zullen heel wat berekeningen moeten uitvoeren


## Feedback 29/01

- Wireframes : 
	- ziet er goed uit, duidelijk!
	- kleine opmerking:extensie van jsf pagina's is niet .jsp
- GIT : 
	- Maak zeker een .gitignore file aan, zorg dat gegenereerde gegevens niet mee in de repository komen van het project
	- Kijk gerust naar .gitignore van jsfstarter project op mijn gitlab.