/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.service;

import com.realdolmen.maven.volleyballKlassement.domain.Team;
import com.realdolmen.maven.volleyballKlassement.repository.TeamRepository;
import java.util.List;

/**
 *
 * @author KDLBL62
 */
public class TeamService {

    TeamRepository teamRepository;

    public TeamService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    public Team findTeamById(Long id) {
        return teamRepository.findById(id);
    }

    public void saveTeam(Team team) {
        teamRepository.save(team);
    }

    public void deleteTeam(Long id) {
        teamRepository.delete(id);
    }

    public List<Team> findAllTeams() {
        return teamRepository.findAll();
    }
}
