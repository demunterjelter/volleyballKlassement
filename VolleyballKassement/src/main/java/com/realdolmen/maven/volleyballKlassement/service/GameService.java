/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.service;

import com.realdolmen.maven.volleyballKlassement.domain.Game;
import com.realdolmen.maven.volleyballKlassement.repository.GameRepository;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author KDLBL62
 */
public class GameService {

    GameRepository gameRepository;

    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public Game findGameById(Long id) {
        return gameRepository.findById(id);
    }

    public void saveGame(Game game) {
        gameRepository.save(game);
    }

    public void deleteGame(Long id) {
        gameRepository.delete(id);
    }

    public List<Game> findAllGames() {
        return gameRepository.findAll();
    }

    public List<Game> findAllGamesFromTeam(Long id) {
        List<Game> gamesTeam = new ArrayList<>();
        List<Game> games = findAllGames();
        for (Game g : games) {
            if (g.getThuisPloeg().getId().equals(id)) {
                gamesTeam.add(g);
            } else if (g.getUitPloeg().getId().equals(id)) {
                gamesTeam.add(g);
            }
        }
        return gamesTeam;
    }

    public int calculateSetsWonByTeam(Long id) {
        List<Game> games = findAllGamesFromTeam(id);
        int aantal = 0;
        for (Game g : games) {
            if (g.getThuisPloeg().getId().equals(id)) {
                aantal += g.getResultaatThuisploeg();
            } else if (g.getUitPloeg().getId().equals(id)) {
                aantal += g.getResultaatUitploeg();
            }
        }
        return aantal;
    }

    public int calculateSetsLostByTeam(Long id) {
        List<Game> games = findAllGamesFromTeam(id);
        int aantal = 0;
        for (Game g : games) {
            if (g.getThuisPloeg().getId().equals(id)) {
                aantal += g.getResultaatUitploeg();
            } else if (g.getUitPloeg().getId().equals(id)) {
                aantal += g.getResultaatThuisploeg();
            }
        }
        return aantal;
    }

    public int getNumberGamesFromTeam(Long id) {
        return findAllGamesFromTeam(id).size();
    }

    public int getNumber3_0_Or_3_1WonGamesFromTeam(Long id) {
        List<Game> games = findAllGamesFromTeam(id);
        int aantal = 0;
        for (Game g : games) {
            if (g.getThuisPloeg().getId().equals(id) && g.getResultaatThuisploeg() == 3 && (g.getResultaatUitploeg() == 0 || g.getResultaatUitploeg() == 1)) {
                aantal += 1;
            } else if (g.getUitPloeg().getId().equals(id) && g.getResultaatUitploeg() == 3 && (g.getResultaatThuisploeg() == 0 || g.getResultaatThuisploeg() == 1)) {
                aantal += 1;
            }
        }
        return aantal;
    }

    public int getNumber3_2WonGamesFromTeam(Long id) {
        List<Game> games = findAllGamesFromTeam(id);
        int aantal = 0;
        for (Game g : games) {
            if (g.getThuisPloeg().getId().equals(id) && g.getResultaatThuisploeg() == 3 && g.getResultaatUitploeg() == 2) {
                aantal += 1;
            } else if (g.getUitPloeg().getId().equals(id) && g.getResultaatUitploeg() == 3 && g.getResultaatThuisploeg() == 2) {
                aantal += 1;
            }
        }
        return aantal;
    }
    
    public int getNumber3_0_Or_3_1LostGamesFromTeam(Long id) {
        List<Game> games = findAllGamesFromTeam(id);
        int aantal = 0;
        for (Game g : games) {
            if (g.getThuisPloeg().getId().equals(id) && g.getResultaatUitploeg() == 3 && (g.getResultaatThuisploeg() == 0 || g.getResultaatThuisploeg() == 1)) {
                aantal += 1;
            } else if (g.getUitPloeg().getId().equals(id) && g.getResultaatThuisploeg() == 3 && (g.getResultaatUitploeg() == 0 || g.getResultaatUitploeg() == 1)) {
                aantal += 1;
            }
        }
        return aantal;
    }

    public int getNumber3_2LostGamesFromTeam(Long id) {
        List<Game> games = findAllGamesFromTeam(id);
        int aantal = 0;
        for (Game g : games) {
            if (g.getThuisPloeg().getId().equals(id) && g.getResultaatUitploeg() == 3 && g.getResultaatThuisploeg() == 2) {
                aantal += 1;
            } else if (g.getUitPloeg().getId().equals(id) && g.getResultaatThuisploeg() == 3 && g.getResultaatUitploeg() == 2) {
                aantal += 1;
            }
        }
        return aantal;
    }
    
    public int calculatePointsFromTeam(Long id){
        int score = 0;
        score += getNumber3_2LostGamesFromTeam(id)*1;
        score += getNumber3_2WonGamesFromTeam(id)*2;
        score += getNumber3_0_Or_3_1WonGamesFromTeam(id) *3;
        return score;
    }
}
