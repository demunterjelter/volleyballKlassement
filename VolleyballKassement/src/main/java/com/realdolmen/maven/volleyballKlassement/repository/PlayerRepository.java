/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.repository;

import com.realdolmen.maven.volleyballKlassement.domain.Player;
import javax.persistence.EntityManager;

/**
 *
 * @author demun
 */
public class PlayerRepository extends AbstractRepository<Player, Long>{
    
    public PlayerRepository(EntityManager em) {
        super(em, Player.class);
    }
    
}