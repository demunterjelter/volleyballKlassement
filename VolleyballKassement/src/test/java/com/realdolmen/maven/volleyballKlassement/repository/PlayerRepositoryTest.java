/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.repository;

import com.realdolmen.maven.volleyballKlassement.domain.Player;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author demun
 */
public class PlayerRepositoryTest{
    private static EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    private static PlayerRepository playerRepository;
    
    @BeforeClass
    public static void initClass() {
        entityManagerFactory = Persistence.createEntityManagerFactory("VolleyballPersistenceUnit");
    }

    @Before
    public void init() {
        entityManager = entityManagerFactory.createEntityManager();
        playerRepository = new PlayerRepository(entityManager);
    }
    
    @Test
    public void findPlayerByIdTest(){
        Player player = playerRepository.findById(1000L);
        assertNotNull(player);
    }
    
    @Test
    public void savePlayerTest(){
        Player player = new Player();
        player.setFirstName("Jelter");
        player.setLastName("Demunter");
        playerRepository.save(player);
        
        Player p1 = entityManager.find(Player.class, player.getId());
        assertEquals("Jelter", p1.getFirstName());
    }
    
    @Test
    public void updatePlayerTest(){
        Player p = entityManager.find(Player.class,2000L);
        playerRepository.begin();
        entityManager.detach(p);
        p.setFirstName("Mats");
        entityManager.flush();
        entityManager.clear();
        playerRepository.update(p);
        
        assertEquals("Mats", entityManager.find(Player.class, 2000L).getFirstName());
    }
    
    @Test
    public void findAllPlayerTest(){
        List<Player> players = playerRepository.findAll();
        assertFalse(players.isEmpty());
    }
    
     @Test
    public void deleteClubTest(){
         playerRepository.delete(3000L);
         assertEquals(null, entityManager.find(Player.class,3000L));
    }
    
    @After
    public void exit() {
        playerRepository.close();
    }

    @AfterClass
    public static void exitClass(){
        entityManagerFactory.close();
    }
    
}