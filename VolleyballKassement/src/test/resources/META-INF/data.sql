
insert into competition(id, active, division, naam, region) values(1000, true, 'eerste provinciale', 'bv-1ste provinciale A', 'vlaams-brabant');
insert into competition(id, active, division, naam, region) values(2000, true, 'tweede provinciale', 'bv-2de provinciale A', 'vlaams-brabant');

insert into club(id, locatie, naam) values(1000, 'lotstraat 39', 'davoc lot');
insert into club(id, locatie, naam) values(2000, 'keerbergenstraat 20', 'fc keerbergen');
insert into club(id, locatie, naam) values(3000, 'zuunstraat 20', 'fc zuun');
insert into club(id, locatie, naam) values(4000, 'leuvenstraat 20', 'fc kut leuven');

insert into team(id, naam, club_id, competition_id) values(1000, 'Lot', 1000, 1000);
insert into team(id, naam, club_id, competition_id) values(2000, 'Keerbergen', 2000, 1000);
insert into team(id, naam, club_id, competition_id) values(3000, 'Zuun', 3000, 1000);


insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (1000, '1996-08-06', 'Lot', 3, 0, 1000, 1000, 2000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (2000, '1998-08-06', 'Lot', 3, 0, 1000, 1000, 2000);

insert into player(id, birthDate, firstName, lastName, team_id) values(1000, '1996-08-06', 'Jelter', 'Demunter', 1000);
insert into player(id, birthDate, firstName, lastName, team_id) values(2000, '1990-05-02', 'Mats', 'Buelinx', 1000);
insert into player(id, birthDate, firstName, lastName, team_id) values(3000, '1987-10-10', 'Jan', 'Jansens', 2000);
insert into player(id, birthDate, firstName, lastName, team_id) values(4000, '1992-09-07', 'Pieter', 'Pieters', 2000);